import { Component, ChangeDetectorRef } from '@angular/core';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera/ngx';
import { ActionSheetController, Platform, ToastController } from '@ionic/angular';
import { FilePath } from '@ionic-native/file-path/ngx';
import { File, FileEntry, IWriteOptions } from '@ionic-native/file/ngx/index';
import { DomSanitizer } from '@angular/platform-browser';
var pdf
// import pdfMake from 'pdfmake/build/pdfmake';
// import pdfFonts from 'pdfmake/build/vfs_fonts';
// pdfMake.vfs = pdfFonts.pdfMake.vfs;
declare var cordova:any;    //global;

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  images: any[];
  lastimage: string;
  image_data: any;
  preparetogobackground: any;
  showerror: any;
  newpathimage: any;

  constructor(
    private camera: Camera,
    private actionSheetController: ActionSheetController, 
    private plt: Platform, 
    private filePath: FilePath,
    public file :File,
    private toastController: ToastController, 
    private sanitizer: DomSanitizer,
    private ref: ChangeDetectorRef, 

    ) { }

    async capture() {
      this.images = [];
      const actionSheet = await this.actionSheetController.create({
        header: "Select Image source",
        buttons: [{
          text: 'Load image from Library',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'capture image using Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
        ]
      });
      await actionSheet.present();
    }
    takePicture(sourceType: PictureSourceType) {
      var options: CameraOptions = {
        quality: 100,
        sourceType: sourceType,
        saveToPhotoAlbum: false,
        correctOrientation: true
      };
      this.camera.getPicture(options).then(imagePath => {
        this.newpathimage = imagePath
        console.log(imagePath)
        // this.photochooes = 'data:image/jpeg;base64,' + imagePath;
        if (this.plt.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
          this.filePath.resolveNativePath(imagePath)
            .then(filePath => {
              let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
              let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
              let smext = currentName.split('.').pop();
              let ext = smext.toLowerCase();
              this.copyFileToLocalDir(correctPath, currentName, this.createFileName(ext));
            });
        } else {
          var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
          var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
          let smext = currentName.split('.').pop();
          let ext = smext.toLowerCase();
          this.copyFileToLocalDir(correctPath, currentName, this.createFileName(ext));
        }

      });
    }
    createFileName(ext) {
      var d = new Date(),
        n = d.getTime(),
        newFileName = n + "." + ext;
      this.lastimage = newFileName
      return newFileName;
  
    }
    copyFileToLocalDir(namePath, currentName, newFileName) {
      this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
        this.updateStoredImages(newFileName);
      }, error => {
        this.presentToast('Error while storing file.');
      });
    }
    updateStoredImages(name) {
      let filePath = this.file.dataDirectory + name;
      let resPath = this.pathForImage(filePath);
      let newEntry = {
        name: name,
        path: resPath,
        filePath: filePath
      };
      this.images.push(newEntry)
      this.image_data=resPath;
      this.ref.detectChanges(); // trigger change detection cycle
    }
    getImgContent() {
      return this.sanitizer.bypassSecurityTrustUrl(this.image_data);
  }
    pathForImage(img) {
      if (img === null) {
        return '';
      } else {
        let converted = (<any>window).Ionic.WebView.convertFileSrc(img);
        return converted;
      }
    }
    async presentToast(text) {
      const toast = await this.toastController.create({
        message: text,
        position: 'bottom',
        duration: 3000
      });
      toast.present();
    }
    createpdf(){
      let options = {
        documentSize: 'A4',
        type: 'base64'
      }
        var file = this.image_data.replace('http://localhost/_app_file_/','file:///android_asset/')
      cordova.plugins.pdf.fromURL(file, {
        documentsize: 'a4',
        landscape: 'portrait',
        type: 'share'
    })
    .then(res =>
    console.log(res)
    
    )
    .catch((err)=>console.log(err))
}
// printInternalFile(param) {

//   /* generate pdf using url. */
//   if(cordova.platformId === 'ios') {

//     // To use window.resolveLocalFileSystemURL, we need this plugin https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-file/
//     // You can add this by doing cordova plugin add cordova-plugin-file or
//     // cordova plugin add https://github.com/apache/cordova-plugin-file
//     window.resolveLocalFileSystemURL(cordova.file.applicationDirectory,
//       (url) => {
//         var file = this.image_data.replace('file:///android_asset/',url.nativeURL);

//         pdf.fromURL(file, {
//             documentsize: 'a4',
//             landscape: 'portrait',
//             type: 'share'
//         })
//           .then((stats)=> this.preparetogobackground )
//           .catch((err)=> this.showerror)
//       },
//       (err) =>
//       console.log('error', err, '  args ->', arguments)
//     );
//   }else {
//         pdf.fromURL(param, {
//             documentsize: 'a4',
//             landscape: 'portrait',
//             type: 'share'
//         })
//           .then((stats)=> this.preparetogobackground )
//           .catch((err)=> this.showerror)
//   }
// }
}